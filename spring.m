%MatLab applications in physics
%Author: Dawid Lazaj
%Technical Physics

%material used is Stainless Steel; source: www.engineeringtoolbox.com
%Shear Modulus  = 11.2e6 psi
%               = 77.2e9Pa
%               = F*l/(A*dx)

%inch2met       = inch/39.370

clc;
clear;

G = 77.2e9;%Pa
r = 2e-3;%m
R = 2/39.370;%m
N = 2:1:50;%range 2-50
omega = 20:0.1:600;
m = 1e-2;%kg
tau = 2;%s
beta = 1./(2*tau);
alfa_0 = 9.81;%m/s^2

%Acquiring k - SpringConstant(N)
%k is later used to calculate omega_0 - natural frequency
%omega_0 is required to get Amplitudes
%among them we look for extreme values and calculate resonance frequencies
for n = 1:size(N, 2)
    k = G*r^4/(4*N(n)*R^3);
    omega_0 = sqrt(k/m);
    for t = 1:size(omega, 2)
        y(t, n) = alfa_0/sqrt((omega_0^2-omega(t)^2)^2+4*beta^2*omega(t)^2);
    end
    %following method based on Rafal Osadnik's work
    [A(n), index] = max(y(:,n));
    fRes(n) = omega(index)/(2*pi);
end

subplot(211);
plot(omega/(2*pi), y);
xlim([10, 55]);
title('Resonance Curves');
xlabel('Frequency [Hz]');
ylabel('Amplitude [m]');

subplot(212);
scatter(N, A);
title('Resonance Amplitudes');
xlabel('Number of Coils');
ylabel('Amplitude [m]');

saveas(gcf, 'resonance.pdf');


dataFile = fopen('resonance_analysis_results.dat', 'w');
fprintf(dataFile,'Source: www.engineeringtoolbox.com\nMaterial used is Stainless Steel\nWire Radius=%.3f[m]\nSpring Coil Radius=%.2f[m]\nMass=%.2f[kg]\nDamping Time Constant=%f.0[s]\nAlpha0=%.2f[m/s^2]\nShear Modulus=%.2f[GPa]\n\n',r,R,m,tau,alfa_0,G/(1e9));
for m = 1:size(N,2)  
	fprintf(dataFile,'N=%.0f\t\tResonance Amplitude=%f[m]\t\tResonance Frequency=%f[Hz]\n',N(m),A(m),fRes(m));
end

fclose(dataFile);
 




